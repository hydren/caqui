package logica.propriedades;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import logica.Atomo;
import logica.formula.Formula;

public class TestadorContradição 
{
	private List<Atomo> atomosEnvolvidos;
	
	private String valoracaoComprovadora;
	
	public boolean serContradicao(Formula f) throws Exception
	{
		setValoracaoComprovadora(null);
		atomosEnvolvidos = new ArrayList<Atomo>(f.determinaAtomosEnvolvidos()); //determina os atomos envolvidos, para otimizar
		
		int nComb = (int) Math.pow(2, atomosEnvolvidos.size()); //determina a quantidade de combinacoes diferentes dos valores dos atomos (2^n)
				
		for(int k = 0; k < nComb; k++) //para cada valoracao possivel (cada k, 0<k<nComb, possui representacao binaria equivalente a uma das possiveis valoracoes do conjunto de atomos envolvidos)
		{
			StringBuilder strValoracao = new StringBuilder(Integer.toBinaryString(k)).reverse(); //pega a repesentacao binaria de k (reversa para começar a iteracao da esquerda para direita)
			while(strValoracao.length() < atomosEnvolvidos.size()) strValoracao.append("0"); //completa com zeros a representacao binaria ('toBinaryString()' coloca só a quantidade necessaria de zeros) 
			HashMap<Atomo, Boolean> valoracao = new HashMap<Atomo, Boolean>(); //cria map com a valoracao da iteracao atual
			for(int i = 0; i < strValoracao.length(); i++)
				valoracao.put(atomosEnvolvidos.get(i), strValoracao.charAt(i)=='1'); //preenche a valoracao, cada atomo recebe true ou false de acordo com o bit de k ('1' para true e '0' para false)
			
			if(f.valorar(valoracao) == true) //se a valoracao da iteracao atual fizer a formula verdadeira, já retorna que a formula não é contradicao
			{
				setValoracaoComprovadora(strValoracao.toString());
				return false;
			}
		}
		
		return true; //caso nenhuma das iteracoes tinha uma valoracao que tornava a formula verdadeira, devemos concluir que a formula é contradicao
	}

	public String getValoracaoComprovadora() {
		return valoracaoComprovadora;
	}

	public void setValoracaoComprovadora(String valoracaoComprovadora) {
		this.valoracaoComprovadora = valoracaoComprovadora;
	}
	
}
