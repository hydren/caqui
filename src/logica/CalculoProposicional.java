package logica;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class CalculoProposicional
{
	/** Conjunto de caracteres validos como atomos neste cálculo proposicional. Definido por padrão.*/
	public Set<Atomo> simbolosProposicionais;
	
	/** Define os operadores suportados atualmente. */
	public static
	enum Operador
	{
		DISJUNCAO, CONJUNCAO, NEGACAO, IMPLICACAO, EQUIVALENCIA
	}
	
	/** Indica símbolos de operadores ou conectivos lógicos de cada operador lógico. */
	public 
	Map<Operador, String> conectivosLogicos;
	
	/** Cria um calculo proposicional com parametros padrão:
	 *<br><br>  * Conjunto de símbolos proposicionais padrão. Contem todas as letras de A a Z maiúsculas.
	 *<br><br>  * Símbolos de operadores padrão. Estes são:
	 *<br> <b>¬</b> Negação
	 *<br> <b>^</b> Conjunção
	 *<br> <b>v</b> Disjunção
	 *<br> <b>-></b> Implicação
	 *<br> <b><-></b> Equivalencia
	 *  */
	public CalculoProposicional()
	{
		simbolosProposicionais = SIMBOLOS_PROPOSICIONAIS_PADRAO;
		conectivosLogicos = CONECTIVOS_LOGICOS_PADRAO;
	}
	
	//CONSTANTES
	
	/** Conjunto de símbolos proposicionais padrão. Contem todas as letras de A a Z maiúsculas. */
	public static final 
	Set<Atomo> SIMBOLOS_PROPOSICIONAIS_PADRAO = Atomo.Utils.converte(new char[]
		{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K',	'L', 
		'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'}
	);
	
	/** Símbolos de operadores padrão. Estes são:
	 *<br> <b>¬</b> Negação
	 *<br> <b>^</b> Conjunção
	 *<br> <b>v</b> Disjunção
	 *<br> <b>-></b> Implicação
	 *<br> <b><-></b> Equivalencia */
	public static final
	Map<Operador, String> CONECTIVOS_LOGICOS_PADRAO = new HashMap<Operador, String>();
	static
	{
		CONECTIVOS_LOGICOS_PADRAO.put(Operador.NEGACAO, "¬");
		CONECTIVOS_LOGICOS_PADRAO.put(Operador.DISJUNCAO, "v");
		CONECTIVOS_LOGICOS_PADRAO.put(Operador.CONJUNCAO, "^");
		CONECTIVOS_LOGICOS_PADRAO.put(Operador.IMPLICACAO, "->");
		CONECTIVOS_LOGICOS_PADRAO.put(Operador.EQUIVALENCIA, "<->");
	}

}
