package logica;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
 
/** Representa um símbolo de proposição, também chamado de variável proposicional ou simplesmente átomo. */
public 
class Atomo implements Comparable<Atomo>, Serializable
{
	private static final long serialVersionUID = -5549392631489432229L;
	
	public 
	String rotulo;
	
	/** Cria um Atomo com o rotulo especificado */
	public Atomo(String rotulo)
	{
		this.rotulo = rotulo;
	}
	
	/** Cria um Atomo com o rotulo especificado */
	public Atomo(char rotulo)
	{
		this.rotulo = ""+rotulo;
	}
	
	/** Classe com utilidades para a class Atomo */
	static public 
	class Utils
	{
		/** Obtem uma lista de atomos com rótulos correspondente aos simbolos passados como parâmetro */
		static public 
		Set<Atomo> converte(char[] simbolos)
		{
			HashSet<Atomo> atomos = new HashSet<Atomo>();
			for(char c : simbolos)
				atomos.add(new Atomo(""+c));
			
			return atomos;
		}
		
		/** Obtem uma lista de atomos com rótulos correspondente aos simbolos passados como parâmetro */
		static public 
		Set<Atomo> converte(Character[] simbolos)
		{
			HashSet<Atomo> atomos = new HashSet<Atomo>();
			for(Character c : simbolos)
				atomos.add(new Atomo(""+c));
			
			return atomos;
		}
		
		/** Obtem uma lista de atomos com rótulos correspondente aos simbolos passados como parâmetro */
		static public 
		Set<Atomo> converte(String[] simbolos)
		{
			HashSet<Atomo> atomos = new HashSet<Atomo>();
			for(String c : simbolos)
				atomos.add(new Atomo(c));
			
			return atomos;
		}
		
		/** Obtem uma lista de atomos com rótulos correspondente aos simbolos passados como parâmetro */
		static public 
		Set<Atomo> converte(Collection<String> simbolos)
		{
			HashSet<Atomo> atomos = new HashSet<Atomo>();
			for(String c : simbolos)
				atomos.add(new Atomo(""+c));
			
			return atomos;
		}
	}
	
	@Override public
	String toString()
	{
		return rotulo;
	}

	@Override public 
	int compareTo(Atomo o) 
	{
		return rotulo.compareTo(o.rotulo);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((rotulo == null) ? 0 : rotulo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Atomo other = (Atomo) obj;
		if (rotulo == null) {
			if (other.rotulo != null)
				return false;
		} else if (!rotulo.equals(other.rotulo))
			return false;
		return true;
	} 
	
	

}
