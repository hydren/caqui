package logica.formula;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.swing.tree.DefaultMutableTreeNode;

import logica.Atomo;
import logica.CalculoProposicional;

public class FormulaNegacao  extends Formula
{
	Formula operando;
	
	public FormulaNegacao(CalculoProposicional calculo, Formula operando) {
		super(calculo);
		this.operando = operando;
	}

	@Override
	public boolean valorar(Map<Atomo, Boolean> valoracao) throws Exception
	{
		return ! operando.valorar(valoracao);
	}

	@Override
	public DefaultMutableTreeNode toNode() 
	{
		DefaultMutableTreeNode node = new DefaultMutableTreeNode("Neg");
		node.add(operando.toNode());
		return node;
	}
	
	@Override
	public String toString() {
		return 	calculoProposicional.conectivosLogicos.get(CalculoProposicional.Operador.NEGACAO)
				+operando.toString();
	}

	@Override
	public String toString(boolean comParenteses) {
		if(comParenteses)
			return	'('+calculoProposicional.conectivosLogicos.get(CalculoProposicional.Operador.NEGACAO)
					+operando.toString(true)+')';
		else
			return toString();
	}

	@Override
	public Set<Atomo> determinaAtomosEnvolvidos() {
		HashSet<Atomo> conjuntoDosAtomosPresentes = new HashSet<Atomo>();
		conjuntoDosAtomosPresentes.addAll(operando.determinaAtomosEnvolvidos());
		return conjuntoDosAtomosPresentes;
	}
}
