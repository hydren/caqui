package logica.formula;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.swing.tree.DefaultMutableTreeNode;

import logica.Atomo;
import logica.CalculoProposicional;

public class FormulaImplicacao extends Formula {
	
	Formula primeiro_operando, segundo_operando;
	
	public FormulaImplicacao(CalculoProposicional calculo, Formula operando1, Formula operando2)
	{
		super(calculo);
		this.primeiro_operando = operando1;
		this.segundo_operando = operando2;
	}

	@Override
	public boolean valorar(Map<Atomo, Boolean> valoracao) throws Exception 
	{
		return (!primeiro_operando.valorar(valoracao)) || segundo_operando.valorar(valoracao);
	}

	@Override
	public DefaultMutableTreeNode toNode() 
	{
		DefaultMutableTreeNode node = new DefaultMutableTreeNode("Impl");
		node.add(primeiro_operando.toNode());
		node.add(segundo_operando.toNode());
		return node;
	}
	
	@Override
	public String toString() {
		return 	primeiro_operando.toString()
				+calculoProposicional.conectivosLogicos.get(CalculoProposicional.Operador.IMPLICACAO)
				+segundo_operando.toString();
	}

	@Override
	public String toString(boolean comParenteses) {
		if(comParenteses)
			return	'(' + primeiro_operando.toString(true)
					+calculoProposicional.conectivosLogicos.get(CalculoProposicional.Operador.IMPLICACAO)
					+segundo_operando.toString(true) + ')';
		else
			return toString();
	}

	@Override
	public Set<Atomo> determinaAtomosEnvolvidos() {
		HashSet<Atomo> conjuntoDosAtomosPresentes = new HashSet<Atomo>();
		conjuntoDosAtomosPresentes.addAll(primeiro_operando.determinaAtomosEnvolvidos());
		conjuntoDosAtomosPresentes.addAll(segundo_operando.determinaAtomosEnvolvidos());
		return conjuntoDosAtomosPresentes;
	}
}
