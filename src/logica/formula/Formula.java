package logica.formula;

import java.util.Map;
import java.util.Set;

import javax.swing.tree.DefaultMutableTreeNode;

import logica.Atomo;
import logica.CalculoProposicional;

public abstract class Formula 
{
	/** Contem os conjuntos de simbolos proposicionais e operadores */
	protected 
	CalculoProposicional calculoProposicional;
	
	public 
	Formula(CalculoProposicional calculo){
		calculoProposicional = calculo;
	}
	
	/** Retorna a instancia de cálculo proposicional cuja formula se baseia. */
	public
	CalculoProposicional getCalculoProposicional()
	{
		return calculoProposicional;
	}
	
	/** Computa o valor verdade dessa fórmula, dependendo da valoração especificada. */
	abstract public 
	boolean valorar(Map<Atomo, Boolean> valoracao) throws Exception;
	
	/** Retorna uma estrutura de nós compatíveis com JTree que representa a árvore de fórmulas que esta fórmula representa. */
	abstract public
	DefaultMutableTreeNode toNode();
	
	/** Retorna a representação da formula em String. */
	abstract public
	String toString();
	
	/** Retorna a representação da formula em String, com parenteses explicitando a precedencia. */
	abstract public
	String toString(boolean comParenteses);
	
	/** Retorna um conjunto de atomos envolvidos nessa formula. Em outras palavras retorna um conjunto de atomos que aparecem na formula. */
	abstract public
	Set<Atomo> determinaAtomosEnvolvidos();
}
