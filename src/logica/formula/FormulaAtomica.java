package logica.formula;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.swing.tree.DefaultMutableTreeNode;

import logica.Atomo;
import logica.CalculoProposicional;

public class FormulaAtomica extends Formula
{
	Atomo atomo;
	
	public FormulaAtomica(CalculoProposicional calculo, Character atomo)
	{
		super(calculo);
		this.atomo = new Atomo(atomo);
	}

	@Override
	public boolean valorar(Map<Atomo, Boolean> valoracao) throws Exception 
	{
		final Boolean resultado = valoracao.get(atomo);
		if(resultado == null)
			throw new Exception("Fórmula atômica possui simbolo proposicional \""+atomo+"\" sem valoração definida!");
		else return resultado;
	}

	@Override
	public DefaultMutableTreeNode toNode() {
		return new DefaultMutableTreeNode(atomo.toString());
	}

	@Override
	public String toString() {
		return atomo.toString();
	}

	@Override
	public String toString(boolean comParenteses) {
		return toString();
	}

	@Override
	public Set<Atomo> determinaAtomosEnvolvidos() {
		HashSet<Atomo> conjuntoUnitario = new HashSet<Atomo>();
		conjuntoUnitario.add(this.atomo);
		return conjuntoUnitario;
	}

}
