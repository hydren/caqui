package modelo;

import java.util.Map;

import logica.Atomo;
import logica.CalculoProposicional;
import logica.formula.Formula;

public class ModeloProposicional 
{
	public
	CalculoProposicional calculo;
	
	public
	Formula formula;
	
	public
	Map<Atomo, Boolean> valoracao;
	
	public ModeloProposicional(CalculoProposicional calculo, Formula formula, Map<Atomo, Boolean> valoracao) 
	{
		this.calculo = calculo;
		this.formula = formula;
		this.valoracao = valoracao;
	}
	
	public ModeloProposicional()
	{
		calculo = new CalculoProposicional();
	}
}
