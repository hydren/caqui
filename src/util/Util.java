package util;

import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

public class Util {
	
	/** Tenta usar o tema do sistema */
	public static void usaTemaDoSistema()
	{
		try
		{
			if(System.getProperty("os.name").equalsIgnoreCase("Linux")) //if Linux, attempt to use GTK+
			{
				for(LookAndFeelInfo lfi : UIManager.getInstalledLookAndFeels())
				{
					if(lfi.getName().equalsIgnoreCase("GTK+"))
					{
						UIManager.setLookAndFeel(lfi.getClassName());
						break;
					}
				}
			}
			else UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch(Exception e){}
	}

	/** Usa o tema padrao do java */
	public static void usaTemaPadrao()
	{
		try
		{
			UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
		}
		catch(Exception e){}
	}
}
