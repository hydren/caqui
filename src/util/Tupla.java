package util;

public class Tupla<T1, T2> 
{
	public T1 termo1;
	public T2 termo2;
	
	public Tupla(T1 t1, T2 t2)
	{
		this.termo1 = t1;
		this.termo2 = t2;
	}

}
