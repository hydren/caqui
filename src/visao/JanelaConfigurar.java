package visao;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.TreeSet;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import logica.Atomo;
import logica.CalculoProposicional.Operador;
import modelo.ModeloProposicional;
import util.Util;
import controle.Config;

public class JanelaConfigurar extends JDialog implements ActionListener
{
	private static final long serialVersionUID = 5155277909614819087L;
	private JTextField txtNegacao;
	private JTextField txtConjuno;
	private JTextField txtDisjuncao;
	private JTextField txtImplicacao;
	private JTextField txtEquivalencia;
	private JCheckBox chckbxUsarTemaDo;
	private JTextArea txtrVariaveis;
	private JButton btnOk;
	private JButton btnAplicar;
	private JButton btnCancelar;
	private JButton btnTrocarNegacao;
	private JButton btnTrocarConjuncao;
	private JButton btnTrocarDisjuncao;
	private JButton btnTrocarImplicacao;
	private JButton btnTrocarEquivalencia;
	private JCheckBox chckbxPerguntarAoFechar;
	
	ModeloProposicional modelo;
	private JButton btnPadro;
	private Component horizontalGlue;
	private Component horizontalStrut;
	private Component horizontalStrut_1;
	private Component horizontalStrut_2;
	private Component horizontalStrut_3;
	private Component verticalStrut;
	private Component verticalStrut_1;
	
	public JanelaConfigurar(JFrame owner, ModeloProposicional m, int tab)
	{
		super(owner);
		BorderLayout borderLayout = (BorderLayout) getContentPane().getLayout();
		borderLayout.setVgap(3);
		setMinimumSize(new Dimension(400, 300));
		setSize(new Dimension(450, 300));
		setTitle("Configuração");
		setModal(true);
		setLocationRelativeTo(null);
		modelo = m;
		
		JPanel panel_botoes = new JPanel();
		getContentPane().add(panel_botoes, BorderLayout.SOUTH);
		
		btnOk = new JButton("Ok");
		btnOk.setPreferredSize(new Dimension(64, 25));
		btnOk.addActionListener(this);
		panel_botoes.setLayout(new BoxLayout(panel_botoes, BoxLayout.X_AXIS));
		
		horizontalStrut_2 = Box.createHorizontalStrut(20);
		horizontalStrut_2.setPreferredSize(new Dimension(10, 0));
		panel_botoes.add(horizontalStrut_2);
		
		btnPadro = new JButton("Padrão");
		panel_botoes.add(btnPadro);
		
		horizontalGlue = Box.createHorizontalGlue();
		panel_botoes.add(horizontalGlue);
		panel_botoes.add(btnOk);
		
		btnAplicar = new JButton("Aplicar");
		btnAplicar.addActionListener(this);
		
		horizontalStrut = Box.createHorizontalStrut(20);
		horizontalStrut.setPreferredSize(new Dimension(10, 0));
		panel_botoes.add(horizontalStrut);
		panel_botoes.add(btnAplicar);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(this);
		
		horizontalStrut_1 = Box.createHorizontalStrut(20);
		horizontalStrut_1.setPreferredSize(new Dimension(10, 0));
		panel_botoes.add(horizontalStrut_1);
		panel_botoes.add(btnCancelar);
		
		horizontalStrut_3 = Box.createHorizontalStrut(20);
		horizontalStrut_3.setPreferredSize(new Dimension(10, 0));
		panel_botoes.add(horizontalStrut_3);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		getContentPane().add(tabbedPane, BorderLayout.CENTER);
		
		JPanel panel_geral = new JPanel();
		tabbedPane.addTab("Geral", null, panel_geral, null);
		panel_geral.setLayout(new GridLayout(2, 2, 0, 0));
		
		chckbxUsarTemaDo = new JCheckBox("Usar tema do sistema");
		chckbxUsarTemaDo.addActionListener(this);
		chckbxUsarTemaDo.setSelected(Config.usaTemaSistema);
		panel_geral.add(chckbxUsarTemaDo);
		
		chckbxPerguntarAoFechar = new JCheckBox("Perguntar ao fechar programa");
		chckbxPerguntarAoFechar.addActionListener(this);
		chckbxPerguntarAoFechar.setSelected(Config.perguntaParaSair);
		panel_geral.add(chckbxPerguntarAoFechar);
		
		JPanel panel_alfa = new JPanel();
		tabbedPane.addTab("Variáveis", null, panel_alfa, null);
		panel_alfa.setLayout(new BoxLayout(panel_alfa, BoxLayout.Y_AXIS));
		
		verticalStrut = Box.createVerticalStrut(20);
		verticalStrut.setPreferredSize(new Dimension(0, 5));
		panel_alfa.add(verticalStrut);
		
		JLabel lblAsVariveisVlidas = new JLabel("<html>As variáveis válidas como átomos. Devem ser apenas 1 caractere. Colocar separadas por virgula</html>");
		lblAsVariveisVlidas.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblAsVariveisVlidas.setFont(new Font("Dialog", Font.BOLD, 12));
		panel_alfa.add(lblAsVariveisVlidas);
		
		verticalStrut_1 = Box.createVerticalStrut(20);
		verticalStrut_1.setPreferredSize(new Dimension(0, 5));
		panel_alfa.add(verticalStrut_1);
		
		JScrollPane scrollPane = new JScrollPane();
		panel_alfa.add(scrollPane);
		
		txtrVariaveis = new JTextArea("");
		txtrVariaveis.setDoubleBuffered(true);
		scrollPane.setViewportView(txtrVariaveis);
		
		String txt_variaveis_buffer="";
		for(Atomo a : new TreeSet<Atomo>(modelo.calculo.simbolosProposicionais))
			txt_variaveis_buffer+=(a.toString()+", ");
		if(txt_variaveis_buffer.length() > 2)
			txt_variaveis_buffer = txt_variaveis_buffer.substring(0, txt_variaveis_buffer.length()-2);
		txtrVariaveis.setText(txt_variaveis_buffer);
		
		JPanel panel_omega = new JPanel();
		tabbedPane.addTab("Operadores", null, panel_omega, null);
		panel_omega.setLayout(new BoxLayout(panel_omega, BoxLayout.Y_AXIS));
		
		JLabel lblOperadores = new JLabel("Operadores");
		lblOperadores.setFont(new Font("Dialog", Font.BOLD, 14));
		panel_omega.add(lblOperadores);
		
		JPanel panel = new JPanel();
		panel_omega.add(panel);
		panel.setLayout(new GridLayout(0, 3, 2, 4));
		
		JLabel lblNegao = new JLabel("Negação");
		panel.add(lblNegao);
		
		txtNegacao = new JTextField();
		txtNegacao.setFont(new Font("Dialog", Font.BOLD, 16));
		panel.add(txtNegacao);
		txtNegacao.setEditable(false);
		txtNegacao.setColumns(10);
		txtNegacao.setText(modelo.calculo.conectivosLogicos.get(Operador.NEGACAO));
		
		btnTrocarNegacao = new JButton("Trocar");
		btnTrocarNegacao.addActionListener(this);
		panel.add(btnTrocarNegacao);
		
		JLabel lblConjuno = new JLabel("Conjunção");
		panel.add(lblConjuno);
		
		txtConjuno = new JTextField();
		txtConjuno.setFont(new Font("Dialog", Font.BOLD, 16));
		txtConjuno.setEditable(false);
		panel.add(txtConjuno);
		txtConjuno.setColumns(10);
		txtConjuno.setText(modelo.calculo.conectivosLogicos.get(Operador.CONJUNCAO));
		
		btnTrocarConjuncao = new JButton("Trocar");
		btnTrocarConjuncao.addActionListener(this);
		panel.add(btnTrocarConjuncao);
		
		JLabel lblDisjuno = new JLabel("Disjunção");
		panel.add(lblDisjuno);
		
		txtDisjuncao = new JTextField();
		txtDisjuncao.setFont(new Font("Dialog", Font.BOLD, 16));
		txtDisjuncao.setEditable(false);
		panel.add(txtDisjuncao);
		txtDisjuncao.setColumns(10);
		txtDisjuncao.setText(modelo.calculo.conectivosLogicos.get(Operador.DISJUNCAO));
		
		btnTrocarDisjuncao = new JButton("Trocar");
		btnTrocarDisjuncao.addActionListener(this);
		panel.add(btnTrocarDisjuncao);
		
		JLabel lblImplicao = new JLabel("Implicação");
		panel.add(lblImplicao);
		
		txtImplicacao = new JTextField();
		txtImplicacao.setFont(new Font("Dialog", Font.BOLD, 16));
		txtImplicacao.setEditable(false);
		panel.add(txtImplicacao);
		txtImplicacao.setColumns(10);
		txtImplicacao.setText(modelo.calculo.conectivosLogicos.get(Operador.IMPLICACAO));
		
		btnTrocarImplicacao = new JButton("Trocar");
		btnTrocarImplicacao.addActionListener(this);
		panel.add(btnTrocarImplicacao);
		
		JLabel lblEquivalncia = new JLabel("Equivalência");
		panel.add(lblEquivalncia);
		
		txtEquivalencia = new JTextField();
		txtEquivalencia.setFont(new Font("Dialog", Font.BOLD, 16));
		txtEquivalencia.setEditable(false);
		panel.add(txtEquivalencia);
		txtEquivalencia.setColumns(10);
		txtEquivalencia.setText(modelo.calculo.conectivosLogicos.get(Operador.EQUIVALENCIA));
		
		btnTrocarEquivalencia = new JButton("Trocar");
		btnTrocarEquivalencia.addActionListener(this);
		panel.add(btnTrocarEquivalencia);
		tabbedPane.setSelectedIndex(tab);
	}

	@Override
	public void actionPerformed(ActionEvent e) 
	{
		Object source = e.getSource();
		
		if(source == null)
			return;
		
		else if(source == btnTrocarConjuncao)
		{
			String resposta = JOptionPane.showInputDialog(this, "Entre com o novo conectivo para conjunção:", txtConjuno.getText());
			if(resposta != null && resposta.trim().equals("")==false)
				txtConjuno.setText(resposta);
		}
		
		else if(source == btnTrocarDisjuncao)
		{
			String resposta = JOptionPane.showInputDialog(this, "Entre com o novo conectivo para disjunção:", txtDisjuncao.getText());
			if(resposta != null && resposta.trim().equals("")==false)
				txtDisjuncao.setText(resposta);
		}
		
		else if(source == btnTrocarEquivalencia)
		{
			String resposta = JOptionPane.showInputDialog(this, "Entre com o novo conectivo para equivalência:", txtEquivalencia.getText());
			if(resposta != null && resposta.trim().equals("")==false)
				txtEquivalencia.setText(resposta);
		}
		
		else if(source == btnTrocarImplicacao)
		{
			String resposta = JOptionPane.showInputDialog(this, "Entre com o novo conectivo para implicação:", txtImplicacao.getText());
			if(resposta != null && resposta.trim().equals("")==false)
				txtImplicacao.setText(resposta);
		}
		
		else if(source == btnTrocarNegacao)
		{
			String resposta = JOptionPane.showInputDialog(this, "Entre com o novo conectivo para negação:", txtNegacao.getText());
			if(resposta != null && resposta.trim().equals("")==false)
				txtNegacao.setText(resposta);
		}
		
		else if(source == btnOk)
		{
			aplicar();
			this.dispose();
		}
		
		else if(source == btnAplicar)
			aplicar();
		
		else if(source == btnCancelar)
			this.dispose();
	}
	
	public void aplicar()
	{
		//preferencias
		if(chckbxUsarTemaDo.isSelected())
			Util.usaTemaDoSistema();
		else
			Util.usaTemaPadrao();
		SwingUtilities.updateComponentTreeUI(this.getOwner());
		
		Config.usaTemaSistema = chckbxUsarTemaDo.isSelected();
		Config.perguntaParaSair = chckbxPerguntarAoFechar.isSelected();
		
		//variaveis
		String[] vars = txtrVariaveis.getText().split(",");
		modelo.calculo.simbolosProposicionais.clear();
		for(String str : vars)
			modelo.calculo.simbolosProposicionais.add(new Atomo(str.trim()));
		
		//conectivos
		modelo.calculo.conectivosLogicos.put(Operador.CONJUNCAO, txtConjuno.getText());
		modelo.calculo.conectivosLogicos.put(Operador.DISJUNCAO, txtDisjuncao.getText());
		modelo.calculo.conectivosLogicos.put(Operador.EQUIVALENCIA, txtEquivalencia.getText());
		modelo.calculo.conectivosLogicos.put(Operador.IMPLICACAO, txtImplicacao.getText());
		modelo.calculo.conectivosLogicos.put(Operador.NEGACAO, txtNegacao.getText());
	}

}
