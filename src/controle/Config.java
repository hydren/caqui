package controle;

import java.awt.Dimension;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

/** Classe com as configurações */
public class Config 
{
	public static 
	boolean usaTemaSistema=true, perguntaParaSair=false;
	
	public static
	Dimension tamanho_tela = new Dimension(640, 480);

	public static
	void carrega() throws FileNotFoundException, IOException
	{
		File file = new File("config.properties");
		if(file.isFile())
		{
			Properties p = new Properties();
			p.load(new FileReader(file));
			
			usaTemaSistema = Boolean.parseBoolean(p.getProperty("usaTemaSistema"));
			perguntaParaSair = Boolean.parseBoolean(p.getProperty("perguntaParaSair"));
			
			try{
				tamanho_tela.width = Integer.parseInt(p.getProperty("tamanho_tela_larg"));
				tamanho_tela.height = Integer.parseInt(p.getProperty("tamanho_tela_alt"));
			}catch(NumberFormatException e)
			{ tamanho_tela = new Dimension(640, 480); }
		}
	}
	
	public static
	void salva() throws IOException, Exception
	{
		File file = new File("config.properties");
		if(file.isDirectory())
			throw new Exception("Não é possível salvar preferências. Um diretório com o nome \"config.properties\"");
		
		file.createNewFile();
		
		Properties p = new Properties();
		p.setProperty("usaTemaSistema", ""+usaTemaSistema);
		p.setProperty("perguntaParaSair", ""+perguntaParaSair);
		
		p.setProperty("tamanho_tela_larg", ""+tamanho_tela.width);
		p.setProperty("tamanho_tela_alt", ""+tamanho_tela.height);
		
		p.store(new FileWriter(file), "caqui-config");
	}
}
