package controle;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JOptionPane;

import util.Util;
import visao.JanelaPrincipal;


public class Main 
{
	public static final 
	String VERSAO = "1.7.0 beta";
	
	/**
	 * @param args Ignorados
	 */
	public static void main(String[] args) 
	{
		try 
		{
			Config.carrega();
		} catch (FileNotFoundException e) {
			//ignora
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Erro ao ler preferencias do aplicativo. "+e.getLocalizedMessage(), "Aviso", JOptionPane.WARNING_MESSAGE);
		}
		if(Config.usaTemaSistema)
			Util.usaTemaDoSistema();
		
		new JanelaPrincipal();
	}

}
